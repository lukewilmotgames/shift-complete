﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; //Need Unity editor

[CustomEditor(typeof(LevelController))]
public class LevelControllerEditor : Editor { //Inherate editor

    public override void OnInspectorGUI() //this is called everytime the inspector is drawn inside of unity
    {      
        LevelController levelController = (LevelController)target; //target is a reference to the script object you are using (LevelController),
                                                                   //However you need to cast it to the approriate datatype (LevelController) in order to use.        

        if (GUILayout.Button("Save"))
        {
            levelController.WriteToFile();
        }

        if (GUILayout.Button("Load"))
        {
            levelController.ReadFile();
        }

        levelController.SwapLevel();
        base.OnInspectorGUI();
    }
}
