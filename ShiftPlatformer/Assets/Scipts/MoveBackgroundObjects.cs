﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackgroundObjects : MonoBehaviour {

    private Vector3 direction;
    private Vector3 rot;
    public float moveSpeed;
    public float rotSpeed;

    void Start()
    {
        //Get a random direction and rotation to move the background objects
        direction = (new Vector3(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f), 0.0f)).normalized;
        rot = (new Vector3(0.0f, 0.0f, Random.Range(-1.0f, 1.0f))).normalized;
    }

	void Update ()
    {
        //Move in those random directions and rotations
        transform.Translate(direction * moveSpeed * Time.deltaTime);
        transform.Rotate(rot * rotSpeed * Time.deltaTime);
    }
}
