﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreThresholds {

    public int level1Score;
    public int level2Score;
    public int level3Score;
    public int level4Score;
}
