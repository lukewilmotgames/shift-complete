﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour {

    private float adTimer;
    private float maxAdTimer = 40f;
    public bool adReadyToPlay = false;

    private void Awake()
    {
        GetSavedAdTimer();
    }

    private void Start()
    {                            //Game ID  
        Advertisement.Initialize("1740865", true);
    }

    private void Update()
    {
        if (!adReadyToPlay)
        {
            adTimer += Time.deltaTime;
        }

        AdTimer();
    }

    public void ShowDefaultAd()
    {                             
        if (Advertisement.IsReady("video"))
        {
            Advertisement.Show("video");
            AdTimerReset();
        }
    }

    public void ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
        }
    }

    private void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }

    private void AdTimer()
    {
        if (adTimer >= maxAdTimer)
        {
            adReadyToPlay = true;
            adTimer = maxAdTimer;
        }
    }

    private void AdTimerReset()
    {
        adTimer = 0f;
    }

    public void SaveAdTimer()
    {
        PlayerPrefs.SetFloat("adTimer", adTimer);
    }

    private void GetSavedAdTimer()
    {
        adTimer = PlayerPrefs.GetFloat("adTimer", adTimer);
    }
}

