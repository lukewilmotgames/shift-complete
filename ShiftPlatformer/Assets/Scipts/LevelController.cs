﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public enum Levels //Levels
    {
        level1,
        level2,
        level3,
        level4,
        level5,
        level6,
        level7,
        level8,
        level9,
        level10,
    }
    public Levels level; //Current Level

    public LevelData levelData; //Level Data

    public Dictionary<string, GameObject> piecesDictionary = new Dictionary<string, GameObject>();

    [HideInInspector] //NEED TO ADD SPACES INTO THIS LIST BEFORE IT SAVES
    public LevelDataName levelDataName; //Second List that stores the gameobjects name
    
    private string levelFilePath; //Level Data Path

    private string levelfile; // level file name

    public List<GameObject> levelPieces = new List<GameObject>(); //All current level pieces in scene

    GameObject currentPiece; //Current Level Piece

    [HideInInspector]
    public int pieceNum; //Current Piece Number

    [HideInInspector]
    public GameObject currentConnector; //Current Level Piece Connector

    GameObject nextPiece; //The next piece that will be added

    GameObject newConnector; //The connector of the next level piece

    Quaternion pieceRot; //Rotation of the next piece

    public GameObject holder; //Empty game object that holds all level pieces as a parent

    public Text levelCheckTxt;

    public GameObject startingPiece;

    public GameObject holder2;

    private GameObject pieceShadow;

    [HideInInspector]
    public GameObject shadowCurrentConnector;

    private GameObject shadowNewConnector;

    //Currently just reads the first level file on start
    void Start()
    {
        GetDictonaryPieces();
        SwapLevel();
        ReadFile();
    }

    //Used the swap the level and change the file path
    public void SwapLevel()
    {
        switch (level)
        {
            case Levels.level1:
                levelfile = "level1";
                levelFilePath = "/StreamingAssets/level1.json";
                break;

            case Levels.level2:
                levelfile = "level2";
                levelFilePath = "/StreamingAssets/level2.json";
                break;

            case Levels.level3:
                levelfile = "level3";
                levelFilePath = "/StreamingAssets/level3.json";
                break;

            case Levels.level4:
                levelfile = "level4";
                levelFilePath = "/StreamingAssets/level4.json";
                break;

            case Levels.level5:
                levelfile = "level5";
                break;

            case Levels.level6:
                levelfile = "level6";
                break;

            case Levels.level7:
                levelfile = "level7";
                break;

            case Levels.level8:
                levelfile = "level8";
                break;

            case Levels.level9:
                levelfile = "level9";
                break;

            case Levels.level10:
                levelfile = "level10";
                break;
        }
    }

    //Reads the Json string back into Unity
    public void ReadFile()
    {
        //Get the file paht of the level
        //string filePath = Application.dataPath + levelFilePath;

        TextAsset filePath2 = Resources.Load(levelfile) as TextAsset;

        //If the file exists 
        //if (File.Exists(filePath)) 
        if (filePath2 != null)
        {
            //Read the string 
            //string levelDataJson = File.ReadAllText(filePath);

            //Converting the Json string into a Unity object and setting it into the levelData class
            //levelData = JsonUtility.FromJson<LevelData>(levelDataJson);
            levelDataName = JsonUtility.FromJson<LevelDataName>(filePath2.text);

            SetNewLevelPieces();

            //Use LevelDataNames over LevelData and pass it through a dictionary, get the right object 
            //and set that to the respective list.

            //UnityEditor.AssetDatabase.Refresh();

            //levelCheckTxt.text = filePath;
        }
        else
        {
            //levelCheckTxt.text = "File Does not exist";
        }
    }

    //Write the levelData class to a Json string
    public void WriteToFile()
    {
        //Instead of saving the instanceID of each object in each list I need to save the name of each object to the string

        //Search through each list in level Data, and add the name of each gameObject to the levelDataName lists which will be saved out to file
        SaveLevelDataNames();

        //Convert the levelData class to Json
        string levelDataToJson = JsonUtility.ToJson(levelDataName, true);

        //Get the File path ill be saving the Json string too
        string filePath = Application.dataPath + "/Resources/" + levelfile + ".json";

        //Write the Json data to that file
        File.WriteAllText(filePath, levelDataToJson);

#if UNITY_EDITOR
            UnityEditor.AssetDatabase.Refresh();
#endif
    }

    //Get any level pieces in the scene
    public void FindStartPiece()
    {
        foreach (GameObject lvlPieces in GameObject.FindGameObjectsWithTag("LevelPieces"))
        {
            //adds piece to list
            levelPieces.Add(lvlPieces);

            //Sets the starting pieces are the current level piece
            currentPiece = levelPieces[0];

            //gets the connector of that piece
            currentConnector = currentPiece.transform.GetChild(0).Find("Connector").gameObject;

            //gets the level number of the current piece
            pieceNum = currentPiece.GetComponent<levelPieceNum>().lvlNum;

            //Build a level piece at the start point
            //BuildPiece(currentConnector);
        }
    }

    //Generates a random number between 0 and the max number of level pieces available.
    GameObject SelectPiece()
    {
        int r = 0;
        switch (pieceNum)
        {
            //top exit - bottom start
            case 0:
                r = Random.Range(0, levelData.bottomStartPieces.Count);
                nextPiece = levelData.bottomStartPieces[r];
                pieceRot = nextPiece.transform.rotation;
                break;

            //right exit - left start
            case 1:
                r = Random.Range(0, levelData.leftStartPieces.Count);
                nextPiece = levelData.leftStartPieces[r];
                pieceRot = nextPiece.transform.rotation;
                break;

            //bottom exit - top start
            case 2:
                r = Random.Range(0, levelData.topStartPieces.Count);
                nextPiece = levelData.topStartPieces[r];
                pieceRot = nextPiece.transform.rotation;
                break;

            //left exit - right start
            case 3:
                r = Random.Range(0, levelData.rightStartPieces.Count);
                nextPiece = levelData.rightStartPieces[r];
                pieceRot = nextPiece.transform.rotation;
                break;
        }
        return nextPiece;
    }

    //Core level build function
    public void BuildPiece(GameObject connectionPiece)
    {
        //Instantiate that new leBvel piece into the world at the current connectors position
        GameObject newPiece = Instantiate(SelectPiece(), currentConnector.transform.position, pieceRot);     

        //Find the new connector on the new level piece
        newConnector = newPiece.transform.GetChild(0).Find("Connector").gameObject;

        //Find the shadow object of the new level piece;
        pieceShadow = newPiece.transform.Find("Shadow").gameObject;
        //Debug.Log(pieceShadow);

        //Find the new shadow connector on the new level piece
        shadowNewConnector = pieceShadow.transform.Find("ShadowConnector").gameObject;
        //Debug.Log(shadowNewConnector);

        //Places the shadow on the current shadow connector so they line up correctly, and keep the same rotation
        pieceShadow.transform.position = shadowCurrentConnector.transform.position;
        pieceShadow.transform.rotation = pieceRot;

        //Add the new level piece to a list to keep track of them
        levelPieces.Add(newPiece);

        //Set the new connector 
        currentConnector = newConnector;

        //Set the new shadow connector
        shadowCurrentConnector = shadowNewConnector;

        //Gets the new piece number for next level piece`
        pieceNum = newPiece.GetComponent<levelPieceNum>().lvlNum;

        //Sets the parent of the new level piece to the holder object in scene
        newPiece.transform.parent = holder.transform;
    }

    //Deletes the first piece in the levelPieces list
    public void DeletePiece()
    {
        //Find first piece
        GameObject index = levelPieces[0];

        //Remove piece from list
        levelPieces.Remove(index);

        //Remove piece from parent
        //index.transform.parent = holder2.transform;

        //Destory piece
        Destroy(index.gameObject);

        //index.transform.position = new Vector3(-10000, index.transform.position.y, index.transform.position.z);
    }

    //Saves off the names of each level piece in LevelData
    void SaveLevelDataNames()
    {
        levelDataName.bottomStartPieces.Clear();
        levelDataName.leftStartPieces.Clear();
        levelDataName.rightStartPieces.Clear();
        levelDataName.topStartPieces.Clear();

        for (int i = 0; i < levelData.bottomStartPieces.Count; i++)
        {           
            levelDataName.bottomStartPieces.Add(levelData.bottomStartPieces[i].name);
        }

        for (int i = 0; i < levelData.leftStartPieces.Count; i++)
        {
            levelDataName.leftStartPieces.Add(levelData.leftStartPieces[i].name);
        }

        for (int i = 0; i < levelData.rightStartPieces.Count; i++)
        {
            levelDataName.rightStartPieces.Add(levelData.rightStartPieces[i].name);
        }

        for (int i = 0; i < levelData.topStartPieces.Count; i++)
        {
            levelDataName.topStartPieces.Add(levelData.topStartPieces[i].name);
        }
    }

    //Find every Level piece in the scene and add that to the dictionary
    void GetDictonaryPieces()
    {
        foreach (GameObject lvlPieces in GameObject.FindGameObjectsWithTag("StartLevelPieces"))
        {
            //if the dictionary doesnt contain the current level piece
            if (!piecesDictionary.ContainsKey(lvlPieces.name))
            {
                piecesDictionary.Add(lvlPieces.name, lvlPieces);
            }
            //if key already exists then replace it with the current
            else
            {
                piecesDictionary[lvlPieces.name] = lvlPieces;
            }
        }
    }

    //Set the new levelpieces in the levelData
    void SetNewLevelPieces()
    {
        levelData.bottomStartPieces.Clear();
        levelData.leftStartPieces.Clear();
        levelData.rightStartPieces.Clear();
        levelData.topStartPieces.Clear();

        for (int i = 0; i < levelDataName.bottomStartPieces.Count; i++)
        {
            levelData.bottomStartPieces.Add(piecesDictionary[levelDataName.bottomStartPieces[i]]);
        }

        for (int i = 0; i < levelDataName.leftStartPieces.Count; i++)
        {
            levelData.leftStartPieces.Add(piecesDictionary[levelDataName.leftStartPieces[i]]);
        }

        for (int i = 0; i < levelDataName.rightStartPieces.Count; i++)
        {
            levelData.rightStartPieces.Add(piecesDictionary[levelDataName.rightStartPieces[i]]);
        }

        for (int i = 0; i < levelDataName.topStartPieces.Count; i++)
        {
            levelData.topStartPieces.Add(piecesDictionary[levelDataName.topStartPieces[i]]);
        }
    }
}
