﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCamera : MonoBehaviour {

    public Transform target;
    public float smoothTime;
    private Vector3 velocity = Vector3.left;

	void Update ()
    {
        Vector3 targetPosition = target.TransformPoint(transform.position);
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
	}
}
