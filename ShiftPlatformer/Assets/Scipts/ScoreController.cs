﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour {

    public float score;
    public float highScore;
    public Text highscoreTitle;
    public Text highscoreTitleScore;
    public Text scoreText;
    public Text endScoreText;
    public Text endHighScore;
    public Animator anim;
    public bool newHighscore = false;
    public Text endHighScoretxt;

    public Text newHighscoretxt;
    public Text currentHighscoretxt;
    public Text currentHighscoretxt2;


    void Start()
    {
        //At the start of the game, get the highscore
        highScore = PlayerPrefs.GetFloat("Highscore");
        endHighScoretxt.text = highScore.ToString();
        LoadHighscore();
    }

    private void Update()
    {
        if (newHighscore)
        {
            newHighscoretxt.enabled = true;
            currentHighscoretxt.enabled = false;
            currentHighscoretxt2.enabled = false;
        }
        else
        {
            newHighscoretxt.enabled = false;
            currentHighscoretxt.enabled = true;
            currentHighscoretxt2.enabled = true;
        }
    }

    //adds score when called
    public void AddScore()
    {
        score++;
        scoreText.text = score.ToString();
        endScoreText.text = score.ToString();
        anim.Play("ScoreAnimation");
    }

    public void SaveHighscore()
    {
        //save the highscore out
        PlayerPrefs.SetFloat("Highscore", highScore);
    }

    public void LoadHighscore()
    {
        if (highScore > 0)
        {
            highscoreTitle.enabled = true;
            highscoreTitleScore.enabled = true;
            highscoreTitleScore.text = highScore.ToString();
        }
        else
        {
            highscoreTitle.enabled = false;
            highscoreTitleScore.enabled = false;
        }
    }
}
