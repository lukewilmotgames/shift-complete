﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    WorldController wc;
    public GameObject StartScreen;
    public GameObject GameScreen;
    public GameObject EndScreen;

    void Awake()
    {
        wc = FindObjectOfType<WorldController>();
    }

	void Update ()
    {
		if (wc.startScreen)
        {
            StartScreen.gameObject.SetActive(true);
            GameScreen.gameObject.SetActive(false);
            EndScreen.gameObject.SetActive(false);
        }
        else if (wc.gameOver)
        {
            StartScreen.gameObject.SetActive(false);
            GameScreen.gameObject.SetActive(false);
            EndScreen.gameObject.SetActive(true);
        }
        else
        {
            StartScreen.gameObject.SetActive(false);
            GameScreen.gameObject.SetActive(true);
            EndScreen.gameObject.SetActive(false);
        }
    }
}
