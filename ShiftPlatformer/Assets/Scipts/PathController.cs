﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathController : MonoBehaviour {

    LevelData levelData;

    public List<GameObject> levelPieces = new List<GameObject>();

    public ScoreController sc;

    int pieceNum;

    GameObject currentPiece;

    [HideInInspector]
    public GameObject currentConnector;

    GameObject nextPiece;
    GameObject newConnector;

    Quaternion pieceRot;
    public GameObject startPoint;

    public GameObject holder;

    //get any starting level pieces in the list
    public void FindStartPiece()
    {
        foreach (GameObject lvlPieces in GameObject.FindGameObjectsWithTag("LevelPieces"))
        {
            //adds piece to list
            levelPieces.Add(lvlPieces);

            //Sets the starting pieces are the current level piece
            currentPiece = levelPieces[0];

            //gets the connector of that piece
            currentConnector = currentPiece.transform.GetChild(0).Find("Connector").gameObject;

            //gets the level number of the current piece
            pieceNum = currentPiece.GetComponent<levelPieceNum>().lvlNum;

            //Build a level piece at the start point
            BuildPiece(currentConnector);
        }
    }

    //Generates a random number between 0 and the max number of level pieces available.
    GameObject SelectPiece()
    {
        int r = 0;
        switch (pieceNum)
        {
            //top exit - bottom start
            case 0:
                r = Random.Range(0, levelData.bottomStartPieces.Count);
                nextPiece = levelData.bottomStartPieces[r];
                pieceRot = nextPiece.transform.rotation; 
                break;

            //right exit - left start
            case 1:
                r = Random.Range(0, levelData.leftStartPieces.Count);
                nextPiece = levelData.leftStartPieces[r];
                pieceRot = nextPiece.transform.rotation; 
                break;
            
            //bottom exit - top start
            case 2:
                r = Random.Range(0, levelData.topStartPieces.Count);
                nextPiece = levelData.topStartPieces[r];
                pieceRot = nextPiece.transform.rotation; 
                break;
                
            //left exit - right start
            case 3:
                r = Random.Range(0, levelData.rightStartPieces.Count);
                nextPiece = levelData.rightStartPieces[r];
                pieceRot = nextPiece.transform.rotation; 
                break;
        }
        return nextPiece;
    }

    //Core level build function
    public void BuildPiece(GameObject connectionPiece)
    {
        //Instantiate that new leBvel piece into the world at the current connectors position
        GameObject newPiece = Instantiate(SelectPiece(), currentConnector.transform.position, pieceRot);

        //Find the new connector on the new level piece
        newConnector = newPiece.transform.GetChild(0).Find("Connector").gameObject;

        //Add the new level piece to a list to keep track of them
        levelPieces.Add(newPiece);

        //Set the new connector 
        currentConnector = newConnector;

        //Gets the new piece number for next level piece
        pieceNum = newPiece.GetComponent<levelPieceNum>().lvlNum;

        //Sets the parent of the new level piece to the holder object in scene
        newPiece.transform.parent = holder.transform;
    }

    //Deletes the first piece in the levelPieces list
    public void DeletePiece()
    {
        //Find first piece
        GameObject index = levelPieces[0];

        //Remove piece from list
        levelPieces.Remove(index);

        //Remove piece from parent
        index.transform.parent = null;

        //Destory piece
        Destroy(index.gameObject);         
    }
}
