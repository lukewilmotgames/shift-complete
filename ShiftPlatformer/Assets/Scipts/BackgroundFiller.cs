﻿using System.Collections;                       
using System.Collections.Generic;
using UnityEngine;

public class BackgroundFiller : MonoBehaviour {

    public List<GameObject> backgroundObjects = new List<GameObject>();
    public int maxBackGroundObjects;

    public List<Material> backgroundColours = new List<Material>();

    private Mesh planeMesh;
    private Bounds planeBounds;

    private Vector3 prevPos;

    public GameObject holder;

    BackgroundColourController bcc;

    public bool backDone = false;

    private void Awake()
    {
        bcc = FindObjectOfType<BackgroundColourController>();
    }

    void Start ()
    {
        planeMesh = GetComponent<MeshFilter>().mesh;
        planeBounds = planeMesh.bounds;

        prevPos = transform.position;

        for (int i = 0; i < maxBackGroundObjects; i++)
        {
            SpawnObject();
        }
    }
   

    //Spawns a new background object at a random position within the bounds
    void SpawnObject()
    {
        //Get a random background object
        int objectNum = Random.Range(0, backgroundObjects.Count);
        GameObject objectPiece = backgroundObjects[objectNum];

        //Getting the min and max of the bounds
        float minX = gameObject.transform.position.x - gameObject.transform.localScale.x * planeBounds.size.x * 0.5f;
        float minZ = gameObject.transform.position.z - gameObject.transform.localScale.z * planeBounds.size.z * 0.5f;

        //Getting a random position within the bounds
        Vector3 objectPos = new Vector3(Random.Range(minX, -minX), Random.Range(minZ, -minZ), gameObject.transform.position.z);

        //Instantiate the object at that position
        GameObject backObject = Instantiate(objectPiece, objectPos, Quaternion.identity);

        backObject.gameObject.transform.parent = holder.transform;

        backObject.GetComponent<Renderer>().material = backgroundColours[bcc.colourNum];
    }

    void OnTriggerExit(Collider other)
    {
        Vector3 dif = other.transform.position - transform.position;
        other.transform.position = new Vector3(other.transform.position.x - dif.x * 2f, other.transform.position.y - dif.y * 2f, other.transform.position.z);
    }
}
