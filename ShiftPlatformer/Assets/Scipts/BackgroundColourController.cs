﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColourController : MonoBehaviour {

    public GameObject backgroundPlane;

    public List<Material> backgroundColours = new List<Material>();

    public int colourNum;

    void Awake()
    {
        colourNum = Random.Range(0, backgroundColours.Count);
        backgroundPlane.GetComponent<Renderer>().material = backgroundColours[colourNum];
    }
}
