﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour {

    public GameObject player;       

    private Vector3 offset;

    private float smooth = 0f;
    public float smoothAngle;

    public float speed = 1.0F;
    [HideInInspector]
    public float startTime;
    private float journeyLength;
    [HideInInspector]
    public Vector3 offsetRot;
    [HideInInspector]
    public Vector3 rotToo;
    public bool firstDone = false;

    private Vector3 startPos;

    public bool startFollowing = false;

    private Vector3 velocity = Vector3.zero;
    private float zVelocity = 0.0F;

    public GameObject other;

    void Start()
    {
        startPos = transform.position;
        //Calculate and store the offset value by getting the distance between the player's position and camera's position.
        offset = transform.position - player.transform.position;
        offsetRot = transform.eulerAngles - player.transform.eulerAngles;
        startTime = Time.time;
        journeyLength = Vector3.Distance(offset, player.transform.position);
    }

    void Update()
    {
        //Set the position of the camera's transform to be the same as the player's, but offset by the calculated offset distance.
        //transform.position = player.transform.position + offset;
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        //transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, offset.z), fracJourney);
        //transform.position = Vector3.SmoothDamp(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, offset.z), ref velocity, smooth);
        //transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -20);
        //transform.eulerAngles = player.transform.eulerAngles;
        Vector3 target = new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z - 20);

        if (!firstDone)
        {
            //transform.position = Vector3.SmoothDamp(transform.position, target, ref velocity, smooth);
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, offset.z), fracJourney);
            transform.eulerAngles = Vector3.Slerp(offsetRot, rotToo, fracJourney);
        }
        else
        {
            //transform.parent = other.transform;
           transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, player.transform.position.y, offset.z), fracJourney);           
           //transform.position = Vector3.SmoothDamp(transform.position, target, ref velocity, smooth);
           
           float xAngle = Mathf.SmoothDampAngle(transform.eulerAngles.x, player.transform.eulerAngles.x, ref zVelocity, smooth);
           float yAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, player.transform.eulerAngles.y, ref zVelocity, smooth);
           float zAngle = Mathf.SmoothDampAngle(transform.eulerAngles.z, player.transform.eulerAngles.z, ref zVelocity, smooth);
           transform.eulerAngles = new Vector3(xAngle, yAngle, zAngle);    
        }
    }

}
