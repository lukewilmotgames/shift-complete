﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildTrigger : MonoBehaviour {

    TransitionController tc;
    bool trigger = true;
    public bool DontSpawnPiece = false;

    void Awake()
    {
        tc = FindObjectOfType<TransitionController>();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (trigger && other.gameObject.tag == "Player")
        {
            if (!DontSpawnPiece)
            {
                tc.BlockTrigger();
            }

            tc.AddScore();

            trigger = false;
        }
    }
}
