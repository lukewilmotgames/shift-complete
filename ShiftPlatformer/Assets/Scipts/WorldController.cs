﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public class WorldController : MonoBehaviour {

    public Rigidbody2D rb;
    public float gravity;
    float gravityProper;
    public float speedBoost;
    public float rotationSpeed;
    public Vector2 velocity;

    Vector3 too;

    float start;
    float jLength;

    Vector3 from;
    bool rotating = false;

    bool pressed = false;

    public float rotationTimer = 0f;


    public Camera camera;

    AdManager ad;

    //The Directions the player can travel in
    public enum Directions
    {
        down,
        right,
        left,
        up,
    }

    public Directions direction;

    public bool playerDead;
    public bool gameOver = false; //used for gameoverUI
    public bool startScreen = true; //used for start screen UI

    LevelController pc;
    public CameraFollowPlayer cfp;
    public StartScreenController ssc;

    public GameObject LeftConnector;
    public GameObject RightConnector;

    private float startTimer = 1f;

    public Animator playerAnim;

    bool startPress = false;

    public ParticleSystem sonar;

    public GameObject shadowConnectorLeft;
    public GameObject shadowConnectorRight;

    public AudioSource audioSource;
    public List<AudioClip> sounds = new List<AudioClip>();

    public AudioSource deathSounds;

    public AudioSource backgroundMusic;

    public GameObject playerShadow;

    public ParticleSystem death;

    public ParticleSystem pulse;

    public ScreenShake ss;

    public GameObject playerPulse;

    AnalyticsManager am;

    void Awake()
    {
        pc = FindObjectOfType<LevelController>();
        am = FindObjectOfType<AnalyticsManager>();
        ad = FindObjectOfType<AdManager>();
    }

    void Update()
    {        
        startTimer -= Time.deltaTime;
        if (startTimer <= 0)
        {
            startTimer = 0;
        }
        
        //if the player is dead then stop their movement
        if (!playerDead)
        {
            gravityProper = gravity;
        }
        else
        {
            gravityProper = 0;
        }

        //update the player gravity to move
        Gravity();

        //rb.velocity = velocity;

        //Lerp setup for the player, calculates the journey 
        float distCovered = (Time.time - start) * rotationSpeed;
        float fracJourney = distCovered / jLength;

        //calculate the result of the lerp
        Vector3 result = Vector3.Lerp(from, too, fracJourney);

        //Only allow one rotation to happen at a time
        if (rotating && fracJourney > 1)
        {
            rotating = false; pressed = false;
        }

        //apply lerp to player
        transform.eulerAngles = result;

        rotationTimer -= Time.deltaTime;

        //Rotation time set to 0 when not needed
        if (rotationTimer <= 0)
        { rotationTimer = 0; }

        if (startTimer <= 0)
        {            
            //Mouse Controls
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                //update the player gravity to move
                PlaySound();
                pulse.Emit(1);
                //if their is not current rotation and the game isnt over or on the title screen then play
                if (!rotating && !pressed && rotationTimer <= 0f && !playerDead && !startScreen && !gameOver)
                {
                    rotationTimer = 0.3f;
                    pressed = true;
                    //Debug.Log("Got a Touch");

                    //gets the touch position
                    var touch = Input.mousePosition;

                    //Check if the touch was on the left side of screen
                    if (touch.x < Screen.width / 2)
                    {
                        Rotate("right");
                        from = transform.rotation.eulerAngles;
                        too = from - new Vector3(0, 0, -90);
                        start = Time.time;

                        cfp.firstDone = true;

                        //Debug.Log("Pressed the Right side of the screen");
                    }

                    //Check if the touch was on the right side of screen
                    else if (touch.x > Screen.width / 2)
                    {
                        Rotate("left");
                        from = transform.rotation.eulerAngles;
                        too = from + new Vector3(0, 0, -90);
                        start = Time.time;

                        cfp.firstDone = true;

                        //Debug.Log("Pressed the Left side of the screen");
                    }
                }

                //if player is on title screen then start game
                else if (gameOver)
                {
                    //do nothing cause the game is over  
                    ad.SaveAdTimer();
                    ssc.ResetPlayer();                   
                    return;
                }
                else
                {
                    playerDead = false;
                    startScreen = false;
                    cfp.startTime = Time.time;
                    if (!startPress)
                    {
                        pc.FindStartPiece();
                        startPress = true;
                    }

                    rotationTimer = 0.3f;
                    pressed = true;
                    //Debug.Log("Got a Touch");
                    //backgroundMusic.Play();

                     //playerAnim.enabled = false;

                     //gets the touch position
                     var touch = Input.mousePosition;

                    //Check if the touch was on the left side of screen
                    if (touch.x < Screen.width / 2)
                    {
                        Rotate("right");
                        from = transform.rotation.eulerAngles;
                        too = from - new Vector3(0, 0, -90);
                        start = Time.time;
                        pc.currentConnector = LeftConnector;
                        pc.pieceNum = 3;

                        cfp.rotToo = cfp.offsetRot - new Vector3(0, 0, -90);

                        Gravity();

                        pc.shadowCurrentConnector = shadowConnectorLeft;

                        //Debug.Log("Pressed the Right side of the screen");
                    }

                    //Check if the touch was on the right side of screen
                    else if (touch.x > Screen.width / 2)
                    {
                        Rotate("left");
                        from = transform.rotation.eulerAngles;
                        too = from + new Vector3(0, 0, -90);
                        start = Time.time;
                        pc.currentConnector = RightConnector;
                        pc.pieceNum = 1;

                        cfp.rotToo = cfp.offsetRot + new Vector3(0, 0, -90);

                        Gravity();

                        pc.shadowCurrentConnector = shadowConnectorRight;

                        //Debug.Log("Pressed the Left side of the screen");
                    }

                }

                jLength = Vector3.Distance(new Vector3(from.x, from.y, from.z), new Vector3(too.x, too.y, too.z));
                rotating = true;
            }
        }
    }

    //this changes the direction the player moves in, based on the Direction enum
    void Gravity()
    {
        switch (direction)
        {
            case (Directions.down):
                velocity = new Vector2(0, -gravityProper);
                break;

            case (Directions.right):
                velocity = new Vector2(gravityProper, 0);
                break;

            case (Directions.left):
                velocity = new Vector2(-gravityProper, 0);
                break;

            case (Directions.up):
                velocity = new Vector2(0, gravityProper);
                break;
        }
        rb.velocity = velocity;
    }

    //This function Updates which direction the player is moving based on which side of the screen was tapped
    void Rotate(string dir)
    {
        switch (dir)
        {
            case ("right"):
                switch (direction)
                {
                    case (Directions.down):
                        direction = Directions.right;
                        break;
                    case (Directions.right):
                        direction = Directions.up;
                        break;
                    case (Directions.up):
                        direction = Directions.left;
                        break;
                    case (Directions.left):
                        direction = Directions.down;
                        break;
                }
                break;

            case ("left"):
                switch (direction)
                {
                    case (Directions.down):
                        direction = Directions.left;
                        break;
                    case (Directions.left):
                        direction = Directions.up;
                        break;
                    case (Directions.up):
                        direction = Directions.right;
                        break;
                    case (Directions.right):
                        direction = Directions.down;
                        break;
                }
                break;
        }
    }

    //Checks if the player collided with a wall and died
    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Wall")
        {
            playerDead = true;
            gameOver = true;
            startTimer = 1f;
            backgroundMusic.Stop();
            audioSource.Stop();
            deathSounds.Play();
            GetComponent<Renderer>().enabled = false;
            playerShadow.GetComponent<Renderer>().enabled = false;
            death.Emit(20);
            cfp.enabled = false;
            camera.transform.parent = this.transform;
            ss.shakeDuration = 1f;
            playerPulse.active = false;
            am.TrackScore();

            if (ad.adReadyToPlay)
            {
                ad.ShowDefaultAd();
            }
        }
    }

    //plays sounds when triggered
    public void PlaySound()
    {
        int i = Random.Range(0, sounds.Count);
        audioSource.clip = sounds[i];
        audioSource.Play();
}
}