﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour {

    //Track PlayTime
    //Track Score

    private float score;

    ScoreController sc;

    private void Awake()
    {
        sc = FindObjectOfType<ScoreController>();
    }

    void OnApplicationQuit()
    {
        Analytics.CustomEvent("Game Over", new Dictionary<string, object>
        {
            {"Play Time", Time.time }
        });
    }

    public void TrackScore()
    {
        score = sc.score;

        Analytics.CustomEvent("Get Score", new Dictionary<string, object>
        {
            {"Score", score }
        });
    }
}
