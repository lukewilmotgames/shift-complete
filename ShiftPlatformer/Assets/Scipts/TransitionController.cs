﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionController : MonoBehaviour {

    LevelController pc;
    ScoreController sc;
    WorldController wc;
    TransitionController tc;

    private int scoreInt = 0;

    int stopEarlyPoints = 2;

    void Awake()    
    {
        pc = FindObjectOfType<LevelController>();
        sc = FindObjectOfType<ScoreController>();
        wc = FindObjectOfType<WorldController>();
        tc = FindObjectOfType<TransitionController>();
    }

    //Used to add a new level piece to the scene
    public void BlockTrigger()
    {
        //Build new level piece
        pc.BuildPiece(pc.currentConnector);

        //if there are more then 3 pieces in the list then delete the first
        if (pc.levelPieces.Count > 3)
        {
            pc.DeletePiece();
        }
    }

    public void AddScore()
    {
        if (stopEarlyPoints <= 0)
        {
            //Add Score
            sc.AddScore();
            stopEarlyPoints = 0;
        }
        else
        {
            stopEarlyPoints--;
        }

        scoreCheck();
    }

    //Used to check the player score
    void scoreCheck()
    {
        //checks if the score has increased by 10 and change the level
        if (sc.score % 10 == 1)
        {
            scoreInt++;
            swapLevelTileSets(scoreInt);
        }

        if (scoreInt > 9)
        {
            scoreInt--;
            swapLevelTileSets(scoreInt);
        }

        if (sc.score > sc.highScore)
        {
            sc.highScore = sc.score;
            sc.SaveHighscore();
            sc.endHighScoretxt.text = sc.highScore.ToString();
            sc.newHighscore = true; 
        }
    }

    //Swap level tilesets
    void swapLevelTileSets(int levelNum)
    {
        switch (levelNum)
        {
            case 1:
                pc.level = LevelController.Levels.level1;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 2:
                pc.level = LevelController.Levels.level2;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 3:
                pc.level = LevelController.Levels.level3;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 4:
                pc.level = LevelController.Levels.level4;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 5:
                pc.level = LevelController.Levels.level5;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 6:
                pc.level = LevelController.Levels.level6;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 7:
                pc.level = LevelController.Levels.level7;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 8:
                pc.level = LevelController.Levels.level8;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 9:
                pc.level = LevelController.Levels.level9;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;

            case 10:
                pc.level = LevelController.Levels.level10;
                pc.SwapLevel();
                pc.ReadFile();
                wc.gravity--;
                break;
        }
    }
}
