﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelData
{
    public List<GameObject> bottomStartPieces = new List<GameObject>();
    public List<GameObject> leftStartPieces = new List<GameObject>();
    public List<GameObject> topStartPieces = new List<GameObject>();
    public List<GameObject> rightStartPieces = new List<GameObject>();
}

[System.Serializable]
public class LevelDataName
{
    public List<string> bottomStartPieces = new List<string>();
    public List<string> leftStartPieces = new List<string>();
    public List<string> topStartPieces = new List<string>();
    public List<string> rightStartPieces = new List<string>();
}



